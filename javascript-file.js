/*
Well done, Gurjeet.  Your application functions Well
and follows most of the business rules.  Great job getting
the regex validations working with the email!
There were a couple that were missed...
- the populateProvinces() function below should have been called loadProvinces()
- the validate() function should have been called validateForm()
- If validation fails on email, the focus isn't set back to the email txtBox.
Otherwise, great work!
17/20
*/
function populateProvinces(){
    //create an array to keep all the provinces's names..
    var provArray=["Alberta","British Columbia","Manitoba","New Brunswick","Newfoundland and Labrador","Nova Scotia","Northwest Territories","Nunavut","Ontario","Prince Edward Island","Quebec","Saskatchewan","Yukon Territory"];
   //a variable to hold the values in a select box..
    var output="<option value=''>-Select-</option>";
    for(index=0;index<provArray.length;index++)
    {
        output+="<option value='"+provArray[index]+"'>"+provArray[index]+"</option>";
    }
    document.getElementById('cboProv').innerHTML=output;
}
function validate(){
  // email format
    var emailFormat = /(\w+\.)*\w+@([a-zA-Z]+\.)+[a-zA-Z]{2,6}/;
    var province = document.getElementById('cboProv').value;
    var name = document.getElementById('txtName').value;
    var email = document.getElementById('txtEmail').value;

    if(province==""){
      //the user has not selected any province
        alert('Please select a province');
        document.getElementById('cboProv').focus();
        return false;
    }

    if(name==""){
      //the user has not entered a name
        alert('Please enter your name');
        document.getElementById('txtName').focus();
        return false;
    }
   if(email==""){
     //the user has not entered an email address..
    alert("Please enter a valid email.");
    return;
    }
    //has not entered an email with a valid format
    else if (!emailFormat.test(email)){  // '!' mean NOT
      alert("please enter a valid email format");
      return;
   }
   //validations passes, form is ready to be submitted..
   alert('Great, Form is succesfully completed');
   }
